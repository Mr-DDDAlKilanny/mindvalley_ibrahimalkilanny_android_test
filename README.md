# Mindvalley_ibrahim_alkilanny_android_test #

Test Library for Android for downloading images async

### What is this repository for? ###

* The cache have a configurable max capacity and evicts images not recently used
* An image load may be cancelled
* The same image may be requested by multiple sources simultaneously
* Multiple distinct resources may be requested in parallel
* Working under the assumption that the same URL will always return the same resource
* The library is easy to integrate into the Pinterest app, and also to any future Android projects
* Solid structure and uses programming design patterns.

### How do I get set up? ###
TODO